import React, { useState, useContext } from 'react';
import AuthAPI from '../services/authAPI';
import AuthContext from '../contexts/AuthContext';

const LoginPage = ({history}) => {
    console.log(history);
    const {setIsAuthenticated} = useContext(AuthContext);
    const [credentials, setCredentials] = useState(
        {
            username: "jacques82@tele2.fr",
            password: "admin"
        }
    );
    const [error, setError] = useState("");

    //Gestion des champs
    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;
        setCredentials({ ...credentials, [name]: value });
    }
    //Gestion du submit 
    const handleSubmit = async (event) => {
        event.preventDefault();

        try {

            await AuthAPI.authenticate(credentials);
            setError("");
            setIsAuthenticated(true);
            /* const data = await CustomersAPI.findAll();
            console.log(data); */
            history.replace("/customers");
        } catch (error) {
            console.log(error.response);
            setError("Compte Inexistant");
        }

        console.log(credentials);

    }
    return (
        <>
            <h1>Connexion à l'application</h1>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="username">Adresse email</label>
                    <input
                        value={credentials.username}
                        onChange={handleChange}
                        type="email"
                        className={"form-control" + (error && " is-invalid")}
                        name="username"
                        id="username"
                        placeholder="Adresse email"
                    />
                    {error && <p className="invalid-feedback">{error}</p>}
                </div>
                <div className="form-group">
                    <label htmlFor="password">Mot de passe</label>
                    <input value={credentials.password} onChange={handleChange} type="password" className="form-control" name="password" id="password" placeholder="mot de passe" />
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-success">Connexion</button>
                </div>
            </form>
        </>
    );
}

export default LoginPage;