import React, { useEffect, useState } from 'react';
import Pagination from "../components/Pagination";
import CustomersAPI from "../services/customersAPI";

const CustomersPage = (props) => {

    const [customers, setCustomers] = useState([]); // customers = etat setCustomers le modifie et on l initialise avec tableau vide
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");

    // Permet d aller chercher les customers
    const fetchCustomers = async () => {
        try {
            const data = await CustomersAPI.findAll()
            setCustomers(data);
        } catch (error) {
            console.log(error.response)
        }
    }
    //Au chargement du composant, on va chercher les customers
    useEffect(() => {//prends en param une fonction à lancer quand klk chose se passe
        fetchCustomers();
    }, []);
    // Gestion de la suppression d'un customers
    const handleDelete = async (id) => {
        const originalCustomers = [...customers];
        //1. approche optimiste on est sur que ca va marcher
        setCustomers(customers.filter(customer => customer.id !== id));
        //2. approche pessimiste 
        try {
            await CustomersAPI.delete(id)
        } catch (error) {
            setCustomers(originalCustomers);//save

        }

    };

    //Gestion de la Recherche
    const handleSearch = ({currentTarget}) => {
        setSearch( currentTarget.value);//save
        setCurrentPage(1);
    }
    const pages = [];
    const itemsPerPage = 10;

    //filtrage des customers en fonction de la recherche
    const filteredCustomers = customers.filter(c => c.firstName.toLowerCase().includes(search.toLocaleLowerCase())
        || c.lastName.toLowerCase().includes(search.toLowerCase())
        || c.email.toLowerCase().includes(search.toLowerCase())
        || (c.compagny && c.compagny.toLowerCase().includes(search.toLowerCase()))



    );
    //d'ou l'on par;t (start) pendant combien  (itemsPerPage)

    //decoupage au nmbre de 10 per page
    //Pagination des données
    const paginatedCustomers = Pagination.getData(filteredCustomers, currentPage, itemsPerPage);

    //Gestion du changement de page    
    const handleChangePage = (page) => setCurrentPage(page);//plusieurs instrcutions on met les accolades
    return (
        <>
            <h1>Liste des clients</h1>
            <div className="form-group">
                <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher" />
            </div>
            <table className="table table-hover ">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Client</th>
                        <th>Email</th>
                        <th>Entrepise</th>
                        <th className="text-center">Factures</th>
                        <th className="text-center">Montant Total</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {/* va retouner pour chaque customer ce tr */}
                    {paginatedCustomers.map(customer => <tr key={customer.id}>
                        <td>{customer.id}</td>
                        <td><a href="#">{customer.firstName} {customer.lastName}</a></td>
                        <td>{customer.email}</td>
                        <td>{customer.compagny}</td>
                        <td className="text-center">
                            <span className="badge badge-primary">{customer.invoices.length}</span>
                        </td>
                        <td className="text-center">{customer.totalAmount.toLocaleString()} £</td>
                        <td>
                            <button
                                onClick={() => handleDelete(customer.id)}
                                disabled={customer.invoices.length > 0}
                                className="btn btn-danger"
                            >Supprimer
                            </button>
                        </td>
                    </tr>)}

                </tbody>
            </table>

            {itemsPerPage < filteredCustomers.length && (<Pagination
                currentPage={currentPage}
                itemsPerPage={itemsPerPage}
                length={filteredCustomers.length}
                onPageChanged={handleChangePage}
            />)}

        </>
    );
}

export default CustomersPage;