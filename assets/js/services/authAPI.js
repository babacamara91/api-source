import Axios from "axios";
import customersAPI from "./customersAPI";
import JwtDecode from "jwt-decode";


function authenticate(credentials) {
    return Axios
        .post("http://127.0.0.1:8000/api/login_check", credentials)
        .then(response => response.data.token)
        .then(token => {

            //stoke le token dans le local Storage
            window.localStorage.setItem("authToken", token);
            //on prévient Axios qu on a maintenant un header par défaut sur ttes nos futures requetes HTTP
            customersAPI.findAll().then(data => console.log(data))
            setAxiosToken(token);
        })
        ;

}

function setAxiosToken(token) {
    Axios.defaults.headers["Authorization"] = "Bearer " + token;

}

function setup() {
    // 1. voir si on a un token ?
    const token = window.localStorage.getItem("authToken");
    // 2. si le token est encore valide 
    if (token) {
        const { exp: expiration } = JwtDecode(token)
        if (expiration * 1000 > new Date().getTime()) {
            setAxiosToken(token);
        }
    }
    // donner le token à axios
}
function isAuthenticated() {
    // 1. voir si on a un token ?
    const token = window.localStorage.getItem("authToken");
    // 2. si le token est encore valide 
    if (token) {
        const { exp: expiration } = JwtDecode(token)
        if (expiration * 1000 > new Date().getTime()) {
            setAxiosToken(token);
            return true;
        }
        return false;
    }
    return false;
}

function logout() {
    window.localStorage.removeItem("authToken");
    delete Axios.defaults.headers["Authorization"]
}


export default {
    authenticate,
    logout,
    setup,
    isAuthenticated
};