import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import { HashRouter, Switch, Route, withRouter } from "react-router-dom";
import CustomersPage from './pages/CustomersPage';
import InvoicesPage from './pages/InvoicesPage';
import LoginPage from './pages/LoginPage';
import authAPI from './services/authAPI';
import AuthContext from "./contexts/AuthContext";
import PrivateRoute from './components/PrivateRoute';
// any CSS you import will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

console.log('Hello Webpack Encore!');

authAPI.setup();


const App = () => {
    //TODO: il faudrait par défaut qu'on demande à notre AuthApi si on est connecté ou pas 
    const [isAuthenticated, setIsAuthenticated] = useState(
        authAPI.isAuthenticated()
    );
    const NavbarWithRouter = withRouter(Navbar);

    return ( //hashrouter genere des routes commençant par #
        //fournir une donnée suivant le composant 
        <AuthContext.Provider value={{
            isAuthenticated,
            setIsAuthenticated
        }}>
            <HashRouter>
                <NavbarWithRouter />

                <main className="container pt-5">
                    <Switch>
                        <Route path="/login" component={LoginPage} />
                        <PrivateRoute path="/invoices" component={InvoicesPage} />
                        <PrivateRoute path="/customers" component={CustomersPage} />
                        <Route path="/" component={HomePage} />
                    </Switch>
                </main>
            </HashRouter>

        </AuthContext.Provider>
        //tous les composants ont accés à ces données

    )
};

//cherche la div
const rootElement = document.querySelector("#app");
ReactDOM.render(<App />, rootElement); // va afficher bonjour React 